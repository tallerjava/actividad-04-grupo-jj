
public class Serie extends Video {

    protected final  int temporada;
   
    public Serie(String titulo, String genero, int anio, int temporada){
        
        super(titulo,genero,anio);
        this.temporada = temporada;
    } 
    
    public Serie(String titulo, String genero, int anio, int temporada, int espectadores){
        
        super(titulo, genero, anio, espectadores);
        this.temporada = temporada;
    } 
    
    @Override
    public String toString() {
        return espectadores + " Espectadores " + " /// "  +  "Serie: " + titulo + " /// " + genero + " /// " + "Año: " + anio + " /// " + temporada + " temporadas";
    }
}
