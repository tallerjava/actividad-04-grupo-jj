
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class Catalogo {
    
    private final List<Video> videos = new ArrayList<>();

    public static void main(String[] args) {
        Catalogo catalogo = new Catalogo();
        catalogo.cargar();
        System.out.println("--------");
        catalogo.mostrarMasTaquillera();
        catalogo.mostrarMenosTaquillera();
        System.out.println("--------");
        catalogo.mostrar();
    }
    
    private void cargar() {
        int minimo = 150;
        int maximo = 10000;
        Random espectadores = new Random();

        videos.add(new Pelicula("Avengers: Infinity War", "Acción", 2018, espectadores.nextInt(maximo - minimo + 1) + minimo));
        videos.add(new Pelicula("Wind River", "Drama", 2017, espectadores.nextInt(maximo - minimo + 1) + minimo));
        videos.add(new Serie("Friends", "Sitcom", 1994, 10, espectadores.nextInt(maximo - minimo + 1) + minimo));
        videos.add(new Serie("Scrubs", "Comedia", 2001, 6, espectadores.nextInt(maximo - minimo + 1) + minimo));
        videos.add(new Pelicula("The Truman Show", "Drama", 1998, espectadores.nextInt(maximo - minimo + 1) + minimo));
        videos.add(new Pelicula("Top Secret!", "Comedia", 1984, espectadores.nextInt(maximo - minimo + 1) + minimo));
        videos.add(new Serie("Breaking Bad", "Drama", 2008, 7, espectadores.nextInt(maximo - minimo + 1) + minimo));
    }

    private void mostrar() {
        videos.forEach(System.out::println);
    }

    private Comparator<Video> compararPorEspectadores() {
        return (Video video1, Video video2) -> video1.espectadores - video2.espectadores;
    }
    
    private Video buscarMasTaquillera() {
        return Collections.max(videos, compararPorEspectadores());
    }
    
    private Video buscarMenosTaquillera() {
        return Collections.min(videos, compararPorEspectadores());
    }
    
    private void mostrarMasTaquillera() {
      Video video = buscarMasTaquillera();
      System.out.println("Más taquillera: " + video.titulo + " (" + video.espectadores + " espectadores)");
    }
    
    private void mostrarMenosTaquillera() {
      Video video = buscarMenosTaquillera();
      System.out.println("Menos taquillera: " + video.titulo + " (" + video.espectadores + " espectadores)");
    }
    /*
    private void ordenarAscendentementePorTitulo() {
        Collections.sort(videos);
    }
    */
}
