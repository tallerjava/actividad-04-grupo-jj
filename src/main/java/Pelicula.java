
public class Pelicula extends Video {

    public Pelicula(String titulo, String genero, int anio) {
        super(titulo, genero, anio);
    }

    public Pelicula(String titulo, String genero, int anio, int espectadores) {
        super(titulo, genero, anio, espectadores);
    }
    
    @Override
    public String toString() {
        return espectadores + " Espectadores" + " /// " + "Título: " + titulo + " /// Género: " + genero + " /// Año: " + anio;
    }
}
