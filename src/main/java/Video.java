
public abstract class Video implements Comparable<Video> {

    protected final String titulo;
    protected final String genero;
    protected final int anio;
    protected int espectadores;    
    
    public Video(String titulo, String genero, int anio) {
        this.titulo = titulo;
        this.genero = genero;
        this.anio = anio;
    }
    
    public Video(String titulo, String genero, int anio, int espectadores) {
        this(titulo, genero, anio);
        this.espectadores = espectadores;
    }

    public String getTitulo() {
        return titulo;
    }
    
    public String getGenero() {
        return genero;
    }

    public int getAnio() {
        return anio;
    }
    
    public int getEspectadores() {
        return espectadores;
    }
    
    public int setEspectadores() {
        return espectadores;
    }
    
    @Override
    public int compareTo(Video video) {
        return this.titulo.compareTo(video.titulo);
    }
}
